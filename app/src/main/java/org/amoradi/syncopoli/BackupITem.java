package org.amoradi.syncopoli;

import java.util.Date;

class BackupItem {
    public String name;
    public String source;
    public String destination;
    public String logFileName;
    public Date lastUpdate;
}